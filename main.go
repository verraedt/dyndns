package main

import (
	"context"
	"os"
	"os/signal"

	"github.com/spf13/cobra"
	"gitlab.com/verraedt/dyndns/updater"

	log "github.com/sirupsen/logrus"
)

func main() {
	var (
		config updater.Config
		debug  bool
	)

	rootCmd := &cobra.Command{
		Use:   "dyndns",
		Short: "Run a dyndns server that updates digitalocean dns",
		Args:  cobra.NoArgs,
		Run: func(cmd *cobra.Command, args []string) {
			if debug {
				log.SetLevel(log.DebugLevel)
			}

			ctx, cancel := context.WithCancel(context.Background())

			c := make(chan os.Signal, 1)
			signal.Notify(c, os.Interrupt)

			go func() {
				<-c
				cancel()
			}()

			config.Context = ctx

			d, err := updater.New(config)
			if err != nil {
				log.Fatal(err)
			}

			err = d.Run()
			if err != nil {
				log.Fatal(err)
			}
		},
	}

	rootCmd.Flags().StringVar(&config.Username, "username", envvar("USERNAME", "noip"), "Username for basic auth")
	rootCmd.Flags().StringVar(&config.PasswordHash, "password-hash", os.Getenv("PASSWORD_HASH"), "Password for basic auth")
	rootCmd.Flags().StringVar(&config.DoToken, "do-token", os.Getenv("DIGITALOCEAN_TOKEN"), "Digitalocean API token, if empty, dns will not be updated")
	rootCmd.Flags().StringVar(&config.DoDomain, "do-domain", envvar("DIGITALOCEAN_DOMAIN", "ext.verraedt.be"), "Digitalocean domain")
	rootCmd.Flags().BoolVarP(&debug, "debug", "v", false, "Verbose output for debugging")

	if err := rootCmd.Execute(); err != nil {
		log.Fatal(err)
	}
}

func envvar(name, fallback string) string {
	if value, ok := os.LookupEnv(name); ok {
		return value
	}

	return fallback
}
