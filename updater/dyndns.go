package updater

import (
	"context"
	"crypto/sha256"
	"fmt"
	"net"
	"net/http"
	"strings"
	"sync"

	log "github.com/sirupsen/logrus"
	"gitlab.com/verraedt/go-dynamic-firewall/util"
)

type Config struct {
	DoToken      string
	DoDomain     string
	Username     string
	PasswordHash string
	context.Context
}

type Updater struct {
	Config
	IPCache    map[string]net.IP
	mux        *http.ServeMux
	sync.Mutex // Lock for IP
}

func New(config Config) (*Updater, error) {
	d := &Updater{
		Config:  config,
		IPCache: make(map[string]net.IP),
		mux:     http.NewServeMux(),
	}

	d.mux.Handle("/nic/update", d)

	return d, nil
}

func (d *Updater) Run() error {
	log.Infof("Running dyndns service for domain %s", d.DoDomain)

	return http.ListenAndServe(":8506", d.mux)
}

func (d *Updater) CheckAccess(username, password string) bool {
	sum := sha256.Sum256([]byte(password))

	return username == d.Username && fmt.Sprintf("%x", sum) == d.PasswordHash
}

func split(fqdn string) (string, string) {
	parts := strings.SplitN(fqdn, ".", 2)

	if len(parts) < 2 {
		return fqdn, ""
	}

	return parts[0], parts[1]
}

func (d *Updater) ServeHTTP(w http.ResponseWriter, r *http.Request) {
	username, password, ok := r.BasicAuth()
	if !ok || !d.CheckAccess(username, password) {
		http.Error(w, "Access denied", http.StatusUnauthorized)

		return
	}

	if system := r.URL.Query().Get("system"); system != "noip" {
		http.Error(w, fmt.Sprintf("Invalid system '%s', expected 'noip'", system), http.StatusForbidden)

		return
	}

	fqdn := r.URL.Query().Get("hostname")

	if fqdn == "" {
		http.Error(w, "Missing hostname", http.StatusForbidden)

		return
	}

	hostname, domain := split(fqdn)

	if domain != d.DoDomain {
		http.Error(w, fmt.Sprintf("Invalid domain '%s', expected '%s'", domain, d.DoDomain), http.StatusForbidden)

		return
	}

	myip := r.URL.Query().Get("myip")
	ip := net.ParseIP(myip)

	if ip == nil {
		http.Error(w, fmt.Sprintf("Invalid ip '%s', could not parse", myip), http.StatusForbidden)

		return
	}

	d.Lock()

	defer d.Unlock()

	if oldIP, ok := d.IPCache[hostname]; ok && ip.Equal(oldIP) {
		http.Error(w, "IP was not changed", http.StatusOK)

		return
	}

	if err := d.Update(hostname, ip); err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)

		return
	}

	d.IPCache[hostname] = ip

	http.Error(w, "DNS update succeeded", http.StatusOK)
}

func (d *Updater) Update(hostname string, ip net.IP) error {
	log.Infof("Setting public address: %v", ip)

	if d.Config.DoToken != "" && d.Config.DoDomain != "" {
		values, err := util.GetAddressRecords([]net.IP{ip})
		if err != nil {
			return err
		}

		return util.UpdateDigitalOcean(d.Config.DoToken, d.Config.DoDomain, hostname, values)
	}

	return nil
}
