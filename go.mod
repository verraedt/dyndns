module gitlab.com/verraedt/dyndns

go 1.16

require (
	github.com/sirupsen/logrus v1.8.1
	github.com/spf13/cobra v1.3.0
	gitlab.com/verraedt/go-dynamic-firewall v0.0.0-20220215085821-52a83e6c7f68
	google.golang.org/appengine v1.6.7 // indirect
)
